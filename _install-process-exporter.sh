#!/bin/bash

# Date: 2021-03-30

sudo mkdir -p /opt/bootstrap

sudo wget https://github.com/ncabatoff/process-exporter/releases/download/v0.7.5/process-exporter-0.7.5.linux-amd64.tar.gz -P /opt/bootstrap

sudo tar -C /opt/bootstrap -xzf /opt/bootstrap/process-exporter-0.7.5.linux-amd64.tar.gz
sudo cp /opt/bootstrap/process-exporter-0.7.5.linux-amd64/process-exporter /usr/local/bin
#sudo chmod 755 /usr/local/bin/process-exporter

sudo mkdir -p /etc/process-exporter
sudo /bin/bash -c 'cat <<EOF > /etc/process-exporter/all.yaml
process_names:
  - name: "{{.Comm}}"
    cmdline:
    - '.+'
EOF'


sudo /bin/bash -c 'cat <<EOF > /usr/lib/systemd/system/process-exporter.service
[Unit]
Description=Process Exporter for Prometheus

[Service]
User=root
Type=simple
ExecStart=/usr/local/bin/process-exporter --config.path /etc/process-exporter/all.yaml --web.listen-address=:9256
KillMode=process
Restart=always

[Install]
WantedBy=multi-user.target
EOF'

#sudo /usr/bin/systemctl daemon-reload
#sudo /usr/bin/systemctl enable process-exporter.service
sudo /usr/bin/systemctl start process-exporter.service
